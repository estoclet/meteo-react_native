import axios from 'axios';
const BASE_URL = 'https://api.openweathermap.org';
const API_KEY = 'b6907d289e10d714a6e88b30761fae22';

export const fetchLocationId = async city => {
  const response = await axios.get(`${BASE_URL}/data/2.5/weather?q=${city}&appid=${API_KEY}`)
  return response.data[0].woeid;
};

export const fetchWeather = async woeid => {
  const response = await axios.get(`${BASE_URL}/${woeid}/`);
  
  const { consolidated_weather, title } = response.data;
  const { the_temp, weather_state_name } = consolidated_weather[0];
  
  return {
    location: title,
    weather: weather_state_name,
    temperature: the_temp,
  };
};
